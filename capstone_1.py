import tensorflow as tf
from scipy.io import loadmat
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Softmax, Conv2D, MaxPooling2D, BatchNormalization, Dropout
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Run this cell to load the dataset
train = loadmat('data/train_32x32.mat')
test = loadmat('data/test_32x32.mat')

# Extract the training and testing images and labels separately from the train and test dictionaries loaded for you.
x_train = train['X']
y_train = train['y']
x_test = test['X']
y_test = test['y']

x_train_gray = np.average(x_train, axis=2)
x_train = np.moveaxis(x_train, -1, 0)
x_test = np.moveaxis(x_train, -1, 0)
print('x_train shape', x_train.shape)
print('x_test shape', x_test.shape)


def display_image(img, label):
    plt.imshow(img)
    plt.title('Image label: ' + str(label))
    plt.show()


def random_image_picker(imgs, labels):
    idx = np.random.randint(imgs.shape[0], size=1)[0]
    img = imgs[idx, :, :]
    display_image(img, labels[idx][0])


def ramdom_plotter(imgs, labels, limit):
    for idx in range(0, limit):
        random_image_picker(imgs, labels)


def random_image_arr(imgs, labels, limit):
    rnd_img_arr = []
    for jdx in range(0, limit):
        idx = np.random.randint(imgs.shape[0], size=1)[0]
        img = imgs[idx, :, :]
        rnd_img_arr.append(img)
    return rnd_img_arr


def get_checkpoint_every_epoch():
    checkpoint = 'checkpoints_every_epoch/checkpoint_{epoch:03d}'
    return ModelCheckpoint(filepath=checkpoint, frequency='epoch', save_weights_only=True, verbose=1)


def get_checkpoint_best_only():
    checkpoint = 'checkpoints_best_only/checkpoint'
    return ModelCheckpoint(filepath=checkpoint, frequency='epoch', save_best_only=True, monitor='sparse_categorical_accuracy',
                           save_weights_only=True, verbose=1)


def get_model_best_epoch(model):
    model.load_weights('checkpoints_best_only/checkpoint')
    return model


checkpoint_every_epoch = get_checkpoint_every_epoch()
checkpoint_best_only = get_checkpoint_best_only()
callbacks = [checkpoint_every_epoch, checkpoint_best_only]


def plot_accuracy_vs_epoch(history):
    plt.plot(history['sparse_categorical_accuracy'])
    plt.title('Accuracy vs. epochs')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='lower right')
    plt.show()


def plot_loss_vs_epoch(history):
    plt.plot(history['loss'])
    plt.title('Loss vs. epochs')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='lower right')
    plt.show()

# MLP neural network classifier


def create_mlp_model(train_images, train_labels):
    print('Defining model')
    model = Sequential([
        Flatten(),
        Dense(512, activation='relu'),
        Dense(64, activation='relu'),
        BatchNormalization(),
        # Dense(64, activation='relu'),
        # Dense(32, activation='relu'),
        Dense(11, activation='softmax')
    ])
    return model

# Build CNN Model


def create_cnn_model(train_images, train_labels):
    print('Defining model', train_images[0].shape)
    model = Sequential([
        Conv2D(8, (3, 3), activation='relu',
               input_shape=train_images[0].shape, padding='SAME'),
        MaxPooling2D(2, 2),
        Flatten(),
        Dense(512, activation='relu'),
        Dense(64, activation='relu'),
        Dense(11, activation='softmax')])
    return model


def fit_model(model, train_images, train_labels):
    print('Compiling model')
    opt = tf.keras.optimizers.Adam(learning_rate=0.005)
    acc = tf.keras.metrics.SparseCategoricalAccuracy()
    mae = tf.keras.metrics.MeanAbsoluteError()
    model.compile(optimizer=opt, loss='sparse_categorical_crossentropy',
                  metrics=[acc, mae])

    print('Fitting model')
    history = model.fit(train_images, train_labels, epochs=30,
                        batch_size=25, callbacks=callbacks)

    print('Done')
    return history

# Color
ramdom_plotter(x_train, y_train, 10)

# Gray scale
x_train_gray_inverted = np.moveaxis(x_train_gray, -1, 0)
ramdom_plotter(x_train_gray_inverted, y_train, 10)

# MLP Model 
my_model = create_mlp_model(x_train, y_train)
my_model.build(input_shape=x_train.shape)
my_model.summary()
history = fit_model(my_model, x_train, y_train)
plot_accuracy_vs_epoch(history.history)
plot_loss_vs_epoch(history.history)

# CNN
my_cnn_model = create_cnn_model(x_train, y_train)
my_cnn_model.build(input_shape=x_train.shape)
my_cnn_model.summary()
history = fit_model(my_cnn_model, x_train, y_train)
plot_accuracy_vs_epoch(history.history)
plot_loss_vs_epoch(history.history)


samples = 5
results = random_image_arr(x_train, y_train, samples)
model_mlp = create_mlp_model(results, y_train)
model_from_saved_weights = get_model_best_epoch(model_mlp)
for i in range(0, samples):
    predictions = model_from_saved_weights.predict(results[i][np.newaxis, ...])
    label_from_prediction = f"Model_prediction: {y_train[np.argmax(predictions)]}"
    display_image(results[i], label_from_prediction)

